# ConsoleCodes

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/ConsoleCodes.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/ConsoleCodes.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/ConsoleCodes.jl/-/pipelines)

Bindings for simple UNIX console control codes, particularly as would be sent to `stdout`
for text editing.  This package is a simple module for storing the basic codes, not a
complete TUI library.

For an explanation and see `man console_codes` or
https://en.wikipedia.org/wiki/ANSI_escape_code .
