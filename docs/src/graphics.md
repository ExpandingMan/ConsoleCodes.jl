```@meta
CurrentModule = ConsoleCodes.Graphics
```

## Console Graphics
```@docs
Graphics
set
reset
bold
italic
strike
blink
reversecolors
underline
foreground
background
```
