```@meta
CurrentModule = ConsoleCodes
```

# ConsoleCodes

This is a module containing a library of [ANSI console control
codes](https://en.wikipedia.org/wiki/ANSI_escape_code).  In linux, a (incomplete) list of
control codes supported by the linux kernel can be found in `man control_codes`.

This package is *not* intended to provide a TUI library, but merely simple convenience
functions for using console control codes.

Note that, since this is considered a "low level" package, it has no exports.
If using it frequently, it is recommended to define `const CC = ConsoleCodes`.

```@contents
```
