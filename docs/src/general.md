```@meta
CurrentModule = ConsoleCodes
```

## General Control Functions
```@docs
backspace
nextstop
carriagereturn
esc
reset
newline
reversefeed
savestate
restorestate
control
insertlines
deletelines
delete
withstyle
printstyled
```
