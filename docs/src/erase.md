```@meta
CurrentModule = ConsoleCodes.Erase
```

## Erasing Printout
```@docs
Erase
fromcursor
tocursor
all
allhistory
linefromcursor
linetocursor
line
```
