using ConsoleCodes
using Documenter

DocMeta.setdocmeta!(ConsoleCodes, :DocTestSetup, :(using ConsoleCodes); recursive=true)

makedocs(;
    modules=[ConsoleCodes],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/ConsoleCodes.jl/blob/{commit}{path}#{line}",
    sitename="ConsoleCodes.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/ConsoleCodes.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "General" => "general.md",
        "Erase" => "erase.md",
        "Cursor" => "cursor.md",
        "Graphics" => "graphics.md",
    ],
)
