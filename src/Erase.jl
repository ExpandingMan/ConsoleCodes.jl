"""
    Erase

Module containing console commands for erasing characters already printed.
"""
module Erase

using ..ConsoleCodes; const CC = ConsoleCodes


"""
    Erase.fromcursor()

Erase console characters from the cursor to the end of the line.
"""
fromcursor(io::IO) = CC.control(io, 'J')
fromcursor() = fromcursor(stdout)

"""
    Erase.tocursor()

Erase console characters from the start of the line to the cursor.
"""
tocursor(io::IO) = CC.control(io, 'J', 1)
tocursor() = tocursor(stdout)

"""
    Erase.all()

Erase the entire console display.
"""
all(io::IO) = CC.control(io, 'J', 2)
all() = all(stdout)

"""
    Erase.allhistory()

Erase the entire console display including scroll-back history.
"""
allhistory(io::IO) = CC.control(io, 'J', 3)
allhistory() = allhistory(stdout)

"""
    Erase.linefromcursor()

Erase from the cursor to the end of the console line.
"""
linefromcursor(io::IO) = CC.control(io, 'K')
linefromcursor() = linefromcursor(stdout)

"""
    Erase.linetocursor()

Erase from the start of the console line to the cursor.
"""
linetocursor(io::IO) = CC.control(io, 'K', 1)
linetocursor() = linetocursor(stdout)

"""
    Erase.line()

Erase the console line.
"""
line(io::IO) = CC.control(io, 'K', 2)
line() = line(stdout)


end
