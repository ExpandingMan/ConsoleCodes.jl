
struct Cursor{ℐ<:IO}
    io::ℐ
end

const cursor = Cursor(stdout)


control(c::Cursor, args...) = control(c.io, args...)

"""
    up(cursor, y)

Move the console cursor up by `y`.

Behavior is undefined for `y ≤ 0` but no check is performed.
See [`Cursor.translate`](@ref) for a safe version of this operation.
"""
up!(c::Cursor, y::Integer) = control(c, 'A', y)

"""
    down!(cursor, y)

Move the console cursor down by `y`.

Behavior is undefined for `y ≤ 0` but no check is performed.
See [`Cursor.translate`](@ref) for a safe version of this operation.
"""
down!(c::Cursor, y::Integer) = control(c, 'B', y)

"""
    right!(cursor, x)

Move the cnosle cursor right by `x`.

Behavior is undefined for `y ≤ 0` but no check is performed.
See [`Cursor.translate`](@ref) for a safe version of this operation.
"""
right!(c::Cursor, x::Integer) = control(c, 'C', x)
 
"""
    left!(cursor, x)

Move the console cursor left by `x`.

Behavior is undefined for `y ≤ 0` but no check is performed.
See [`Cursor.translate`](@ref) for a safe version of this operation.
"""
left!(c::Cursor, x::Integer) = control(c, 'D', x)

"""
    down1!(cursor, y)

Move the console cursor down by `y`, to the start of the line.

Invalid for `y ≤ 0`.
"""
function down1!(c::Cursor, y::Integer)
    if y < 1
        throw(ArgumentError("down1 requires positive definite argument, got $y"))
    end
    control(c, 'E', y)
end

"""
    up1!(cursor, y)

Move the consle cursor up by `y`, to the start of the line.

Invalid for `y ≤ 0`.
"""
function up1!(c::Cursor, y::Integer)
    if y < 1
        throw(ArgumentError("up1 requires positive definite argument, got $y"))
    end
    control(c, 'F', y)
end

"""
    xtranslate!(cursor, x)

Translate the cursor horizontally by `x`.
"""
function xtranslate!(c::Cursor, x::Integer)
    if x > 0
        right!(c, x)
    elseif x < 0
        left!(c, -x)
    end
end

"""
    ytranslate!(cursor, y)

Translate the cursor vertically by `y`.
"""
function ytranslate!(c::Cursor, y::Integer)
    if y > 0
        up!(c, y)
    elseif y < 0
        down!(c, -y)
    end
end

"""
    translate!(cursor, x, y)

Translate the cursor by `x` horizontally and `y` vertically.
"""
function translate!(c::Cursor, x::Integer, y::Integer)
    xtranslate!(c, x)
    ytranslate!(c, y)
end

"""
    tocolumn!(cursor, x)

Move the console cursor to column `x`.
"""
tocolumn!(c::Cursor, x::Integer) = control(c, 'G', y)

"""
    to!(cursor, x, y)

Move the console cursor to position `(x, y)`.
"""
to!(c::Cursor, x::Integer, y::Integer) = control(c, 'H', y, x)

"""
    save(cursor)

Save the cursor position.  The state is saved to an internal OS register that can be
loaded with [`Cursor.restore`](@ref) but is otherwise inaccessible.
"""
save(c::Cursor) = control(c, 's')

"""
    restore(cursor)

Restore the cursor position saved with [`Cursor.save()`](@ref).
"""
restore(c::Cursor) = control(c, 'u')

