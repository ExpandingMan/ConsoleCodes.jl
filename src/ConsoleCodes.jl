module ConsoleCodes


"""
    backspace()

Backspace one column, but not past the beginning of the line.
"""
backspace(io::IO=stdout) = write(io, 0x08)

"""
    nextstop()

Goes to the next tab stop or to the end of the line if there is no earlier tab stop.
"""
nextstop(io::IO=stdout) = write(io, 0x09)

"""
    carriagereturn()

Gives a carriage return.
"""
carriagereturn(io::IO=stdout) = write(io, 0x0d)

"""
    esc(v)

Write control bytes `v` following the start of a console escape sequence.
"""
esc(io::IO, v) = write(io, 0x1b) + write(io, v)
esc(v) = esc(stdout, v)

"""
    reset()

Reset console controls.
"""
reset(io::IO=stdout) = esc(io, "c")

"""
    newline()

Insert a new line using a console escape sequence.
"""
newline(io::IO=stdout) = esc(io, "E")

"""
    reversefeed()

Reverse the console line feed.
"""
reversefeed(io::IO=stdout) = esc(io, "M")

"""
    savestate()

Save the current console state, includes cursor coordinates, attributes and
character sets.  The state is saved to an internal OS register that can be
restored with [`restorestate`](@ref) but is otherwise inaccessible.
"""
savestate(io::IO=stdout) = esc(io, "7")

"""
    restorestate()

Restore the console state saved by [`savestate`](@ref).
"""
restorestate(io::IO=stdout) = esc(io, "8")

"""
    control(c, args...)

Insert a console control sequence with control character `c` and arguments `args`.

Each argument is inserted into the stream with `print`, so typically `Integer` types
should be provided as arguments.
"""
function control(io::IO, c::UInt8, args...)
    esc(io, "[")
    for (i, a) ∈ enumerate(args)
        print(io, a)  # rather unbelievably, it is looking for ASCII representations
        i ≠ length(args) && write(io, ";")
    end
    write(io, c)
    nothing
end
control(io::IO, c::Char, args...) = control(io, UInt8(c), args...)
control(c::Union{Char,UInt8}, args...) = control(stdout, c, args...)

"""
    altscreen(b::Bool)

Enable (`b=true`) or disable (`b=false`) the alternate screen buffer.
"""
altscreen(io::IO, b::Bool) = esc(io, "[?1049"*(b ? 'h' : 'l'))
altscreen(b::Bool) = altscreen(stdout, b)

"""
    insertlines(n)

Insert `n` blank lines using a console escape sequence.
"""
insertlines(io::IO, n::Integer) = control(io, 'L', n)
insertlines(n::Integer) = insertlines(stdout, n)

"""
    deletelines(n)

Delete `n` lines using a console escape sequence.
"""
deletelines(io::IO, n::Integer) = control(io, 'M', n)
deletelines(n::Integer) = deletelines(stdout, n)

"""
    delete(n)

Delete `n` characters on the current line using a console escape sequence.
"""
delete(io::IO, n::Integer) = control(io, 'X', n)
delete(n::Integer) = delete(stdout, n)


include("Erase.jl")
include("cursor.jl")
include("Graphics.jl")


const ColorSpec = Union{Symbol,Int,NTuple{3,Int}}

"""
    withstyle(𝒻, io::IO, ss...;
              color=nothing, bold=false, italic=false, strike=false, blink=false,
              reverse=false, underline=false, background=nothing,
              reset=true
             )

Call `𝒻(io, ss...)` writing console graphics formatting control sequences before and after as specified.

Note that saving the console state does not save graphic attributes, so the console state prior to printing is lost.
If `reset=true`, the console graphical reset [`Graphics.reset()`](@ref) will be called, completely erasing whatever
state the console may have had both prior to and during printing with `printstyled`.

Foreground color is specified with `color`, background color is specified with `background`.  If `nothing`, the console
color state will remain unchanged.  Colors can be specified with any of
- An integer between 0 and 255 (256-color)
- A 3-tuple of integers, each between 0 and 255 (24-bit color).
- One of the following `Symbol`s: `:default, :black, :red, :green, :brown, :blue, :magenta, :cyan, :white`.
"""
function withstyle(𝒻, io::IO, ss...;
                   color::Union{Nothing,ColorSpec}=nothing,
                   bold::Bool=false, italic::Bool=false, strike::Bool=false,
                   blink::Bool=false, reverse::Bool=false, underline::Bool=false,
                   background::Union{Nothing,ColorSpec}=nothing,
                   reset::Bool=true,
                  )
    if !get(io, :color, false)
        𝒻(io, ss...)
        return
    end
    bold && Graphics.bold(io, true)
    italic && Graphics.italic(io, true)
    strike && Graphics.strike(io, true)
    blink && Grpahics.blink(io, true)
    reverse && Graphics.reverse(io, true)
    underline && Graphics.underline(io, true)
    isnothing(color) || Graphics.foreground(io, color)
    isnothing(background) || Graphics.background(io, background)
    𝒻(io, ss...)
    reset && Graphics.reset(io)
    nothing
end

"""
    printstyled(io::IO, ss...; kw...)
    printstyled(ss...; kw...)

Print arguments `ss` with graphical styling provided by console control codes.  Similar to `Base.prinstyled` but with
more features.

If no `io` is specified, `stdout` will be used.

Internally uses [`withstyle`](@ref), see the documentation of that function for a description of all available options.
"""
printstyled(io::IO, ss...; kw...) = withstyle(print, io, ss...; kw...)
printstyled(ss...; kw...) = printstyled(stdout, ss...; kw...)


#TODO: add codes for answer

end
