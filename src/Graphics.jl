"""
    Graphics

Module for setting console graphics options.  Note that many of these are not universally supported.
"""
module Graphics

using ..ConsoleCodes; const CC = ConsoleCodes


"""
    Graphics.set(args...)

Set console graphics options.
"""
set(io::IO, args...) = CC.control(io, 'm', args...)
set(args...) = set(stdout, args...)

"""
    Graphics.reset()

Reset all console graphics options to their defaults.
"""
reset(io::IO) = set(io, 0)
reset() = reset(stdout)

"""
    Graphics.bold(on)

Set (`on=true`) or disable (`on=true`) console graphics printing as bold.
"""
bold(io::IO, on::Bool) = set(io, on ? 1 : 22)
bold(on::Bool) = bold(stdout, on)

"""
    Graphics.italic(on)

Set (`on=true`) or disable (`on=false`) console graphics printing as italic.

Note that, oddly, this is option is not listed in `man console_codes` but seems to be supported by Linux anyway.
"""
italic(io::IO, on::Bool) = set(io, on ? 3 : 23)
italic(on::Bool) = italic(stdout, on)

"""
    Graphics.strike(on)

Set (`on=true`) or disable (`on=false`) console graphics printing with a strike-through.

Note that, oddly, this is option is not listed in `man console_codes` but seems to be supported by Linux anyway.
"""
strike(io::IO, on::Bool) = set(io, on ? 9 : 29)
strike(on::Bool) = strike(stdout, on)

"""
    Graphics.blink(on)

Set (`on=true`) or disable (`on=false`) blinking console graphics.
"""
blink(io::IO, on::Bool) = set(io, on ? 5 : 25)
blink(on::Bool) = blink(stdout, on)

"""
    Graphics.reversecolors(on)

Set (`on=true`) or disable (`on=false`) console printing with inverted colors.
"""
reversecolors(io::IO, on::Bool) = set(io, on ? 7 : 27)
reversecolors(on::Bool) = reversecolors(stdout, on)

"""
    Graphics.underline(on)

Set (`on=true`) or disable (`on=false`) console printing with underlines.
"""
underline(io::IO, on::Bool) = set(io, on ? 21 : 24)
underline(on::Bool) = underline(stdout, on)

"""
    Graphics.foreground(c)
    Graphics.foreground(r, g, b)

Set console graphics to print with foreground color `c`, which can be either a `Symbol` or an integer.  If an integer,
this will set a 256 foreground value.  Alternatively, the integer triplet `r, g, b` can be used to set 24-bit foreground.

Valid `Symbol` options are:
```
:default, :black, :red, :green, :brown, :blue, :magenta, :cayn, :white
```
"""
function foreground(io::IO, c::Symbol)
    x = if c == :default
        39
    elseif c == :black
        30
    elseif c == :red
        31
    elseif c == :green
        32
    elseif c == :brown
        33
    elseif c == :blue
        34
    elseif c == :magenta
        35
    elseif c == :cyan
        36
    elseif c == :white
        37
    else
        throw(ArgumentError("$c is not a valid console foreground color symbol"))
    end
    set(io, x)
end
foreground(io::IO, x::Integer) = set(io, 38, 5, x)
foreground(io::IO, r::Integer, g::Integer, b::Integer) = set(io, 38, 2, r, g, b)
foreground(io::IO, rgb::Tuple) = foreground(io, rgb...)
foreground(args...) = foreground(stdout, args...)

"""
    Graphics.background(c)
    Graphics.background(r, g, b)

Set console graphics to print with background color `c`, which can be either a `Symbol` or an integer.  If an integer,
this will set a 256 background value.  Alternatively, the integer triplet `r, g, b` can be used to set 24-bit background.

Valid `Symbol` options are:
```
:default, :black, :red, :green, :brown, :blue, :magenta, :cayn, :white
```
"""
function background(io::IO, c::Symbol)
    x = if c == :default
        49
    elseif c == :black
        40
    elseif c == :red
        41
    elseif c == :green
        42
    elseif c == :brown
        43
    elseif c == :blue
        44
    elseif c == :magenta
        45
    elseif c == :cyan
        46
    elseif c == :white
        47
    else
        throw(ArgumentError("$c is not a valid console background color symbol"))
    end
    set(io, x)
end
background(io::IO, x::Integer) = set(io, 48, 5, x)
background(io::IO, r::Integer, g::Integer, b::Integer) = set(io, 48, 2, r, g, b)
background(io::IO, rgb::Tuple) = background(io, rgb...)
background(args...) = background(stdout, args...)


end
