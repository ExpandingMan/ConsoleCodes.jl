using ConsoleCodes; const CC = ConsoleCodes
using Test

using ConsoleCodes: Erase, Cursor, Graphics, printstyled


# return what 𝒻 writes to a buffer
function bfc(𝒻)
    io = IOBuffer()
    𝒻(io)
    take!(io)
end

# return what 𝒻 writes to a buffer with color support
function bfcc(𝒻)
    io = IOContext(IOBuffer(), :color=>true)
    𝒻(io)
    take!(io.io)
end


@testset "ConsoleCodes.jl" begin


@testset "control_characters" begin
    @test bfc(CC.backspace) == [0x08]
    @test bfc(CC.nextstop) == [0x09]
    @test bfc(CC.carriagereturn) == [0x0d]

    @test bfc(io -> CC.esc(io, 0x00)) == [0x1b, 0x00]
end

@testset "control_sequences" begin
    @test bfc(io -> CC.control(io, 'a')) == [0x1b, 0x5b, UInt8('a')]
    @test bfc(io -> CC.control(io, 'a', 1)) ==
        [0x1b, 0x5b, UInt8('1'), UInt8('a')]
    @test bfc(io -> CC.control(io, 'a', 1, 2)) ==
        [0x1b, 0x5b, UInt8('1'), UInt8(';'), UInt8('2'), UInt8('a')]
end

@testset "cursor" begin
    @test_throws ArgumentError bfc(io -> CC.down1!(Cursor(io), 0))
    @test_throws ArgumentError bfc(io -> CC.up1!(Cursor(io), 0))
    @test bfc(io -> CC.translate!(Cursor(io), 0, 3)) == bfc(io -> CC.up!(Cursor(io), 3))
    @test bfc(io -> CC.translate!(Cursor(io), -1, 0)) == bfc(io -> CC.left!(Cursor(io), 1))
end

@testset "graphics" begin
    @test bfcc(io -> Graphics.set(io, 1, 2)) ==
        [0x1b, 0x5b, UInt8('1'), UInt8(';'), UInt8('2'), UInt8('m')]
end

@testset "style" begin
    # ensure that we don't do anything if color is not supported
    @test bfc(io -> printstyled(io, "spock", color=:blue)) == codeunits("spock")
    @test bfcc(io -> printstyled(io, "kirk", bold=true, reset=false)) ==
        [0x1b; 0x5b; UInt8('1'); UInt8('m'); codeunits("kirk")]

    rst = bfc(Graphics.reset)  # we'll need this a few times

    @test bfcc(io -> printstyled(io, "bones", italic=true)) ==
        [0x1b; 0x5b; UInt8('3'); UInt8('m'); codeunits("bones"); rst]
    @test bfcc(io -> printstyled(io, "scotty", color=:red)) ==
        [0x1b; 0x5b; codeunits("31m"); codeunits("scotty"); rst]
    @test bfcc(io -> printstyled(io, "uhura", background=255)) ==
        [0x1b; 0x5b; codeunits("48;5;255m"); codeunits("uhura"); rst]
    @test bfcc(io -> printstyled(io, "sulu", color=(128, 128, 0))) ==
        [0x1b; 0x5b; codeunits("38;2;128;128;0m"); codeunits("sulu"); rst]
end


end
